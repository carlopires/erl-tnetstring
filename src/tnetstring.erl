-module(tnetstring).

-export([encode/1, decode/1]).

%% API

encode(null) -> <<"0:~">>;
encode(true) -> <<"4:true!">>;
encode(false) -> <<"5:false!">>;
encode(Number) when is_integer(Number) ->
  Payload = list_to_binary(integer_to_list(Number)),
  Size = list_to_binary(integer_to_list(size(Payload))),
  <<Size/binary, <<":">>/binary, Payload/binary, <<"#">>/binary>>;
encode(Number) when is_float(Number) ->
  Payload = list_to_binary(io_lib:format("~p", [Number])),
  Size = list_to_binary(integer_to_list(size(Payload))),
  <<Size/binary, <<":">>/binary, Payload/binary, <<"^">>/binary>>;
encode(ByteString) when is_binary(ByteString) ->
  Size = list_to_binary(integer_to_list(size(ByteString))),
  <<Size/binary, <<":">>/binary, ByteString/binary, <<",">>/binary>>;
encode(Object) when is_tuple(hd(Object)) ->
  Payload = << <<Bin/binary>> || Bin <- encode_o(Object, []) >>,
  Size = list_to_binary(integer_to_list(size(Payload))),
  <<Size/binary, <<":">>/binary, Payload/binary, <<"}">>/binary>>;
encode(List) when is_list(List) ->
  Payload = << <<Bin/binary>> || Bin <- encode_l(List, []) >>,
  Size = list_to_binary(integer_to_list(size(Payload))),
  <<Size/binary, <<":">>/binary, Payload/binary, <<"]">>/binary>>.

decode(<<Null:3/binary, Remain/binary>>) when Null =:= <<"0:~">> -> {null, Remain};
decode(<<True:7/binary, Remain/binary>>) when True =:= <<"4:true!">> -> {true, Remain};
decode(<<False:8/binary, Remain/binary>>) when False =:= <<"5:false!">> -> {false, Remain};
decode(TNetString) ->
  {Size, UnparsedPayload} = unpact_size(TNetString),
  {Payload, UnparsedTag} = unpact_payload(UnparsedPayload, Size),
  {Tag, Remain} = unpact_tag(UnparsedTag),
  {decode(Tag, Payload), Remain}.

decode(<<"#">>, Payload) -> list_to_integer(binary_to_list(Payload));
decode(<<"^">>, Payload) -> list_to_float(binary_to_list(Payload));
decode(<<",">>, Payload) -> Payload;
decode(<<"]">>, Payload) -> decode_l(Payload, []);
decode(<<"}">>, Payload) -> decode_o1(Payload).

%% Internals

encode_l([], Acc) -> lists:reverse(Acc);
encode_l([Head|Tail], Acc) -> encode_l(Tail, [encode(Head)|Acc]).

encode_o([], Acc) -> lists:reverse(Acc);
encode_o([{}], _Acc) -> [<<>>];
encode_o([{Key, Value}|Tail], Acc) ->
    encode_o(Tail, [encode(Value), encode(Key)|Acc]).

decode_l(<<>>, Acc) -> lists:reverse(Acc);
decode_l(Payload, Acc) ->
    {Term, Remain} = decode(Payload),
    decode_l(Remain, [Term|Acc]).

decode_o1(<<>>) -> [{}];
decode_o1(Payload) -> decode_o2(Payload, []).

decode_o2(<<>>, Acc) -> lists:reverse(Acc);
decode_o2(Payload, Acc) ->
    {Key, Remain} = decode(Payload),
    {Value, Remain2} = decode(Remain),
    decode_o2(Remain2, [{Key, Value}|Acc]).

unpact_size(TNetString) -> unpact_size(TNetString, []).
unpact_size(<<Colon:1/binary, Remain/binary>>, Acc)
  when Colon =:= <<":">> ->
    BinSize = << <<Bin/binary>> || Bin <- lists:reverse(Acc) >>,
    Size = list_to_integer(binary_to_list(BinSize)),
    {Size, Remain};
unpact_size(<<Byte:1/binary, Remain/binary>>, Acc) ->
  unpact_size(Remain, [Byte|Acc]).

unpact_payload(Remain, Size) ->
    <<Payload:Size/binary, Remain2/binary>> = Remain,
    {Payload, Remain2}.

unpact_tag(Remain) ->
    <<Tag:1/binary, UnparsedRemain/binary>> = Remain,
    {Tag, UnparsedRemain}.

