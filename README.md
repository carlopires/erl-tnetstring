erl-tnetstring
==========

Pure Erlang implementation of [TNetStrings](http://tnetstrings.org/) as an OTP library.

This repository is a fork of [erlang tnetstring implementation](https://github.com/tOkeshu/tnetstring.erl) from Romain Gauthier 
packaged as an OTP library and minor changes.

License
----------

[Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).